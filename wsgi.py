from werkzeug.wsgi import DispatcherMiddleware

from kb_ngramviewer import frontend

application = DispatcherMiddleware(frontend.create_app())
