import json

from .. import factory
from . import assets

def create_app(settings_override=None):
    """ Returns the KBNgramViewer frontend application instance"""
    app = factory.create_app(__name__, __path__, settings_override)

    # Init assets
    assets.init_app(app)

    with open('stats.json', 'r') as f:
        app.config['FREQUENCIES'] = json.load(f)

    return app
