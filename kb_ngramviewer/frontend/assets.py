from flask.ext.assets import Environment, Bundle

# CSS
css_all = Bundle('css/bootstrap.css', 'css/app.css', 'css/font-awesome.min.css',
                 filters='cssmin', output='css/app.min.css')
# JS
js_all = Bundle('js/jquery-1.10.2.js', 'js/bootstrap.js', 'js/typeahead.js',
                'js/d3.v3.js', 'js/underscore.js', 'js/backbone.js',
                'js/app.js', filters='closure_js', output='js/app.min.js')

def init_app(app):
    webassets = Environment(app)
    webassets.config['CLOSURE_COMPRESSOR_OPTIMIZATION'] = 'SIMPLE_OPTIMIZATIONS'
    webassets.register('css_all', css_all)
    webassets.register('js_all', js_all)
    webassets.manifest = 'cache' if not app.debug else False
    webassets.cache = not app.debug

    if app.config['MINIFY_WEBASSETS']:
        webassets.debug = False
    else:
        webassets.debug = True
