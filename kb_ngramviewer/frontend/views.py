from datetime import datetime
from pytz import timezone

import json
import re
import requests

from flask import (Blueprint, render_template, Response, current_app, request,
                   redirect, url_for)

from ..core import NgramviewerError
from ..services import elasticsearch


bp = Blueprint('frontend', __name__)

@bp.route('/')
def index():
    return render_template('index.html', stats=stats())


@bp.route('/search/<term>')
def search(term):
    term = term.strip()

    ngram_size = len(term.split())

    if ngram_size > 5:
        raise NgramviewerError('This ngram consists of more than 5 terms')

    # This seems to be the fastest solution
    query = {
        "query": {
            "constant_score": {
               "filter": {
                    "term": {
                       "term": term
                    }
               }
            }
        }
    }
    # Tried:
    # query = {
    #     'filter': {
    #         'term': {
    #             'term': term
    #         }
    #     }
    # }
    #
    # query = {
    #     'query': {
    #         'match_phrase': {
    #             'term': term
    #         }
    #     },
    #     'size': 1
    # }

    res = elasticsearch.search(query=query, index='%dgram' % ngram_size)

    if res['hits']['hits']:
        years = res['hits']['hits'][0]['_source']['years']
        freqs = current_app.config.get('FREQUENCIES', None)
        if not freqs:
            raise NgramviewerError('No file with term frequencies per year '\
                                   'was found.')

        years = [{
            'freq': year['count'],
            'date': '%d-01-01' % year['year'],
            'rel_freq': float(year['count']) / freqs[u'%dgrams' % ngram_size][str(year['year'])]['tokens']
        } for year in years if year['year'] >= current_app.config.get('MIN_YEAR', 1840)]

        return Response(json.dumps(years), mimetype='application/json')
    else:
        return Response('[]', mimetype='application/json')


@bp.route('/suggest/<q>')
def suggest(q):
    # Return 5 suggestions by default, use size param to customize
    size = int(request.args.get('size', 5))
    offset = int(request.args.get('offset', 0))
    query = {
        'term_suggest': {
            'text': q,
            'completion': {
                'field': 'suggest',
                'size': size + offset
            }
        }
    }

    res = elasticsearch.suggest(query=query)
    if res['term_suggest'][0]['options']:
        datums = [{'value': s['text'], 'tokens': s['text'].split()} for s in res['term_suggest'][0]['options'][offset:]]
    elif current_app.config.get('FUZZY_AUTOCOMPLETE', False):
        # If no results are found, try a fuzzy query
        query['term_suggest']['completion']['fuzzy'] = True
        res = elasticsearch.suggest(query=query)
        datums = [{'value': s['text'], 'tokens': s['text'].split()} for s in res['term_suggest'][0]['options'][offset:]]
    else:
        datums = []

    return Response(json.dumps(datums), mimetype='application/json')


@bp.route('/results')
def results():
    """As the KB does not support GET requests on their results endpoint, we
    do a search request here, and render the results page as if it was our own.
    The KB does use absolute URLs, everything works, and users are transparently
    moving to the KB website if they need context.

    However, this is a very, very stoopit solution."""
    q = request.args.get('q', None)
    date_start = request.args.get('date_start', '')
    date_end = request.args.get('date_end', '')
    visitor_key = request.cookies.get(current_app.config.get('ILPSLOGGING_PROJECT_KEY', None))

    # I refuse to use non-ISO formatted dates, so do some parsing
    # Also, strptime does not do stuff before 1900, so do the reversing manually
    date_start = date_start.split('-')
    date_start.reverse()
    date_start = '-'.join(date_start)

    date_end = date_end.split('-')
    date_end.reverse()
    date_end = '-'.join(date_end)

    if not q or not date_end or not date_start:
        return redirect(url_for('.index'))

    payload = {
        'origin': 'advancedsearch',
        'q': q,
        'x': 0,
        'y': 0,
        'date_start': date_start,
        'date_end': date_end,
        'area_of_distribution[all]': 'on',
        'article_type[all]': 'on',
        'titles': 'all',
        'places': 'all'
    }

    r = requests.post('http://kranten.kb.nl/results', data=payload)

    if re.search(r'Er is een fout opgetreden', r.content):
        ams = timezone('Europe/Amsterdam')
        created_at = ams.localize(datetime.now())
        logdata = {
            'created_at': created_at.strftime('%Y-%m-%dT%H:%M:%S.%f%z'),
            'event_type': 'kb_error',
            'event_properties': {
                'query': q,
                'date_start': date_start,
                'date_end': date_end
            },
            'state': {
                'visitor_key': visitor_key
            }
        }
        requests.post(current_app.config.get('ILPSLOGGING_SERVER') + '/api/v0/log',
                      auth=(current_app.config.get('ILPSLOGGING_PROJECT_KEY'), ''), data=json.dumps(logdata))

    return Response(r.content)


def stats():
    """Return some interesting statistics over the indices"""
    res = elasticsearch.stats(metric_family='docs', index='_all')
    stats = {'Total': '{0:,d}'.format(res['_all']['total']['docs']['count']).replace(',', '.')}
    for index, stat in res['indices'].iteritems():
        # Yuck. Don't look here
        index = index[0] + '-' + index[1:-4] + 'men'
        stats[index] = '{0:,d}'.format(stat['total']['docs']['count']).replace(',', '.')
    return stats
