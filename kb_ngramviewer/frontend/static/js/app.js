var script = document.createElement('script');
script.async = true;
script.src = 'http://ilpslogging.staging.dispectu.com/jssdk/ilpslogging-0.2.min.js';
var entry = document.getElementsByTagName('script')[0];
entry.parentNode.insertBefore(script, entry);

var ILPSLogging_ready = ILPSLogging_ready || [];
ILPSLogging_ready.push(function(){
    var config = {
        api_url: 'http://ilpslogging.staging.dispectu.com',
        project_key: 'p58OaVgDrIDzmCjqtIaHcEqhttJn85DuMATXSEmsQsg',
        log_mouse_movements: true,
        log_mouse_clicks: true,
        post_events_queue_on_browser_close: true,
        log_browser_close: true,
        debug: false
    };
    ILPSLogging.init(config, function(){
        console.log('ready for action');
    });
});


(function($){
    var NGram = Backbone.Model.extend();

    var NGramsCollection = Backbone.Collection.extend({
        model: NGram,

        query: function(qs) {
            // TODO: ILPSLogging is not initialized when we enter here from router
            // (i.e. when the user clicked a link /#q/henk)
            // ILPSLogging.query(qs);
            var self = this;
            $.getJSON('/search/' + qs, function(data) {
                if (data.length > 0) {
                    self.add({ id: qs, name: qs, values: data});
                }
            });
        }
    });

    var SearchBarView = Backbone.View.extend({
        el: '#search-bar',

        events: {
            'keypress input#query': 'keypressQuery'
        },

        initialize: function(options) {
            var self = this;

            this.collection.on('add', this.clearInput, this);

            this.$('input').typeahead({
                name: 'ngram',
                remote: 'suggest/%QUERY'
            });

            this.$('input').bind('typeahead:selected', $.proxy(this.autocompleteQuery, this));
            this.$('input#query').focus();
        },

        autocompleteQuery: function(event, query) {
            this.query(query.value);
            if (ILPSLogging) {
                ILPSLogging.query(query.value, {
                    active_queries: this.collection.pluck('id').sort(),
                    initiated_by: 'autocomplete'
                });
            }
        },

        keypressQuery: function(event) {
            var query = this.$('input#query').val();

            if (ILPSLogging) ILPSLogging.logEvent('autococomplete_keypress', {pressed_key: event.keyCode, current_input: query});

            if (event.keyCode == 13) {
                this.query(query.trim());
                if (ILPSLogging) {
                    ILPSLogging.query(query, {
                        active_queries: this.collection.pluck('id').sort(),
                        initiated_by: 'enterpress'
                    });
                }
            }
        },

        query: function(query_string) {
            if (!($('#search').hasClass('searched'))) {
                $('#search').addClass('searched');
                $('#content').css('display', 'block');
            }

            this.collection.query(query_string);
        },

        clearInput: function(item) {
            if (item.get('id') === this.$('input#query').val().trim()) {
                this.$('input#query').typeahead('setQuery', '');
            }
        }
    });

    var LegendView = Backbone.View.extend({
        el: '#legend',

        events: {
            'click li': 'removeItem',
            'mouseenter li': 'hoverOnItem',
            'mouseleave li': 'hoverOffItem'
        },

        initialize: function(options) {
            this.vent = options.vent;

            this.ngrams = {};

            this.collection.on('add', this.addItem, this);
            this.collection.on('change:color', this.changeColor, this);
        },

        addItem: function(ngram) {
            var text = ngram.get('id');

            var legend_item = $('<li class="btn" data-id="' + text + '"><i class="icon-remove"></i> ' + text + '</li>');
            this.$('ul').append(legend_item);

            this.ngrams[text] = legend_item;
        },

        removeItem: function(event){
            var text;
            if (typeof(event) === "object") {
                text = $(event.currentTarget).data('id');
            }
            else {
                text = event;
            }

            this.ngrams[text].remove();
            delete this.ngrams[text];
            this.collection.remove(text);

            if (ILPSLogging) {
                ILPSLogging.logEvent('remove_query', {
                    query: text,
                    active_queries: this.collection.pluck('id').sort()
                });
            }
        },

        hoverOnItem: function(event) {
            var query = $(event.currentTarget).data('id');
            this.vent.trigger('legend.item.hoveron', {'id': query});

            if (ILPSLogging) ILPSLogging.logEvent('legend_hoveron', {'query': query});
        },

        hoverOffItem: function(event) {
            var query = $(event.currentTarget).data('id');
            this.vent.trigger('legend.item.hoveroff', {'id': query});

            if (ILPSLogging) ILPSLogging.logEvent('legend_hoveroff', {'query': query});
        },

        changeColor: function(ngram) {
            this.ngrams[ngram.get('id')].css('background-color', ngram.get('color'));
        }
    });

    var ShareView = Backbone.View.extend({
        el: '#share',

        initialize: function(options){
            this.vent = options.vent;
            this.vent.on('router:URLChange', this.updateTweetIntent, this);
        },

        updateTweetIntent: function(route) {
            console.log(route);
            var url = encodeURIComponent(Backbone.history.location.origin + '/#q/' + encodeURIComponent(route.replace('q/', ''))),
                // ngrams may not be available yet, so do something icky
                ngrams = _.map(route.replace('q/', '').split('|'), function(ngram){ return '"' + ngram + '"'; }),
                text;

            if(ngrams.length < 1){
                // If there are no ngrams, don't show button
                this.$el.empty();
                return;
            }

            if(ngrams.length > 1) {
                text = 'Bekijk de voorkomens van ' + ngrams.slice(0, -1).join(', ') +
                       ' en ' + ngrams.slice(-1) + ' in het KBKranten corpus.';
            } else {
                text = 'Bekijk de voorkomens van ' + ngrams[0] + ' in het KBKranten corpus.';
            }

            console.log(url);

            this.$el.html('<a href="https://twitter.com/intent/tweet?url=' +
                          url + '&text=' + encodeURIComponent(text) + '&hashtags=kbngram" target="_blank"' +
                          ' class="btn btn-primary btn-lg">Share! <span class="icon-twitter icon-large"></span></a>');
        }
    });

    var GraphView = Backbone.View.extend({
        el: '#graph',

        events: {
            'click #freq_toggle': 'toggleFreqType'
        },

        initialize: function(options){
            this.vent = options.vent;

            this.usedColors = {};
            this.availableColors = ['#2980B9', '#27AE60', '#8E44AD', '#F39C12',
                                    '#16A085', '#2C3E50', '#D35400', '#3498DB',
                                    '#2ECC71', '#9B59B6', '#1ABC9C', '#F1C40F',
                                    '#34495E', '#E67E22'];

            this.chart = this.ngramChart().freqType('rel_freq').contextSize(0.275).height(400);

            this.collection.once('add', this.calcDimensions, this);

            this.collection.on('add', function(ngram) {
                this.setColor(ngram);
                d3.select(this.el).datum(this.collection.toJSON()).call(this.chart);
            }, this);

            this.collection.on('remove', function(ngram){
                this.unsetColor(ngram);
                d3.select(this.el).datum(this.collection.toJSON()).call(this.chart);
            }, this);

            this.vent.on('legend.item.hoveron', this.chart.hoverOn);
            this.vent.on('legend.item.hoveroff', this.chart.hoverOff);
        },

        calcDimensions: function() {
            var minWidth = 500;
            var minHeight = 375;
            var maxHeight = 625;

            // Set the width of the graph
            var width = $(this.el).width();
            if (width < minWidth) width = minWidth;

            // Set the height of the chart
            var height = $(window).height() - $(this.el).offset().top;
            if (height < minHeight) {
                height = minHeight;
            }
            else if (height > maxHeight) {
                height = maxHeight;
            }
            this.chart.height(height).width(width);
        },

        setColor: function(ngram) {
            if (!(ngram.get('id') in this.usedColors)) {
                ngram.set('color', this.availableColors.shift());
            }
        },

        unsetColor: function(ngram){
            this.availableColors.unshift(ngram.get('color'));
            delete this.usedColors[ngram.get('id')];
        },

        toggleFreqType: function(){
            // TODO: figure out why the main chart "dips to white", where the
            // context chart does not

            // Switch the frequency type...
            if (this.chart.freqType() === 'rel_freq'){
                this.chart.freqType('freq');
            } else {
                this.chart.freqType('rel_freq');
            }

            // ... and rerender the chart
            d3.select(this.el).datum(this.collection.toJSON()).call(this.chart);
        },

        ngramChart: function() {
            var margin = {top: 20, right: 20, bottom: 170, left: 100},
                margin2 = {top: 470, right: 10, bottom: 20, left: 100},
                width = 760,
                height = 120,
                xValue = function(d) { return d[0]; },
                yValue = function(d) { return d[1]; },
                xScale = d3.time.scale(),
                yScale = d3.scale.linear(),
                xScale2 = d3.time.scale(),
                yScale2 = d3.scale.linear(),
                brush = d3.svg.brush().x(xScale2).on('brush', brushed).on('brushend', function(e){
                    if (ILPSLogging) {
                        if (brush.empty()) {
                            ILPSLogging.logEvent('brush_clear', {
                                relativeStartPos: 0.0,
                                relativeEndPos: 1.0
                            });
                        }
                        else {
                            var range = xScale2.range(),
                                extent = brush.extent();

                            ILPSLogging.logEvent('brush_select', {
                                relativeStartPos: xScale2(extent[0]) / (range[1] - range[0]),
                                relativeEndPos: xScale2(extent[1]) / (range[1] - range[0])
                            });
                        }
                    }
                }),
                xAxis = d3.svg.axis().scale(xScale).orient('bottom').tickSize(6, 0).ticks(d3.time.years, 10).tickPadding(7).tickFormat(d3.time.format('%Y')),
                xAxis2 = d3.svg.axis().scale(xScale2).orient('bottom').ticks(d3.time.years, 20).tickFormat(d3.time.format('%Y')).tickSize(0, 0).tickPadding(5),
                yAxis = d3.svg.axis().scale(yScale).orient('left'),
                line = d3.svg.line().x(X).y(Y).interpolate('monotone'),
                line2 = d3.svg.line().x(X).y(Y2).interpolate('monotone'),
                ISOFormat = d3.time.format('%Y-%m-%d'),
                yearFormat = d3.time.format('%Y'),
                contextSize = 0.33333,
                freqType = 'rel_freq';


            function chart(selection){
                selection.each(function(data){
                    if(freqType === 'freq') yAxis.tickFormat(null);
                    if(freqType === 'rel_freq') yAxis.tickFormat(function(tick){ return tick + '%'; });

                    // If we have brushed on a previous query, remove the brush
                    // on rerender
                    d3.select('.context .brush').call(brush.clear()).call(d3.behavior.drag().on('dragend', function(){
                        ILPSLogging.logEvent('brush_dragged');
                    }));

                    margin.bottom = height * contextSize;
                    margin2.top = (height * (1 - contextSize)) + 30;

                    var minXValue = d3.min(data, function(d){ return d3.min(d.values, function(c){ return ISOFormat.parse(c.date); }); }),
                        maxX = d3.max(data, function(d){ return d3.max(d.values, function(c){ return ISOFormat.parse(c.date); }); }),
                        maxYValue = d3.max(data, function(d){ return d3.max(d.values, function(c){ return c[freqType]; }); }),
                        // The maximum value denotes the absolute top value Y can have. As we interpolate, and draw
                        // circles, some pixels of the lines and circles will be "larger" than the maximum value in the
                        // data. By adding 2% to the max value, these values should always fall within these margins
                        maxY = maxYValue * 1.05,
                        minX = d3.time.month.offset(minXValue, -3);
                        range = d3.time.years(minXValue, d3.time.year.offset(maxX, 1));

                    // Fill in with zeroes for the years when there isn't any data, and reformat for non-
                    // deterministic accessors ([[x1, y1], [x2, y2], ...], rather than an array of objects)
                    ngrams = data.map(function(ngram){
                        var values = range.map(function(d){
                            var datapoint = _.findWhere(ngram.values, {date: ISOFormat(d)});
                            if(datapoint){
                                return [d, datapoint[freqType], ngram.name, datapoint.freq, datapoint.rel_freq, ngram.color];
                            } else {
                                return [d, 0, ngram.name, 0, 0, ngram.color];
                            }
                        });
                        return {name: ngram.name, color: ngram.color, values: values};
                    });

                    xScale
                        .domain([minX, maxX])
                        .range([0, width - margin.left - margin.right]);

                    xScale2
                        .domain(xScale.domain())
                        .range(xScale.range());

                    yScale
                        .domain([0, maxY])
                        .range([height - margin.top - margin.bottom, 0]);

                    yScale2
                        .domain(yScale.domain())
                        .range([height - margin2.top - margin2.bottom, 0]);

                    xAxis.tickSize(-height + margin.top + margin.bottom, 0).ticks(d3.time.years, 10);
                    yAxis.tickSize(-width + margin.left + margin.right, 0);

                    // Select svg element, if it exists
                    var svg = d3.select(this).selectAll('svg').data([ngrams]);

                    // Otherwise, create chart skeleton
                    var svgEnter = svg.enter().append('svg');
                    svgEnter.append('defs').append('clipPath')
                        .attr('id', 'clip')
                      .append('rect')
                        .attr('width', width)
                        .attr('height', height );

                    var gEnter = svgEnter.append('g').attr('class', 'inner');
                    gEnter.append('g').attr('class', 'x axis');
                    gEnter.append('g').attr('class', 'y axis');

                    var contextEnter = svgEnter.append('g').attr('class', 'context');

                    // Update outer dimensions
                    svg.attr('width', width)
                       .attr('height', height);

                    // Update inner dimensions
                    var g = svg.select('g.inner')
                        .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

                    var context = svg.select('g.context')
                        .attr('transform', 'translate(' + margin2.left + ',' + margin2.top + ')');

                    // Select line elements, if they exist
                    var lines = d3.select('g.inner').selectAll('g.term').data(ngrams, function(d){ return d.name; });
                    var contextLines = d3.select('g.context').selectAll('g.term').data(ngrams, function(d){ return d.name; });

                    var paths = lines.select('path.line');
                    // Apply simple transition on existing lines (e.g. do not redraw them)
                    paths.transition()
                        .attr('d', function(d){ return line(d.values); })
                        .attr('stroke-dashoffset', null)
                        .attr('stroke-dasharray', null);

                    var contextPaths = contextLines.select('path.line');
                    // Apply simple transition on existing lines (e.g. do not redraw them)
                    contextPaths.transition()
                        .attr('d', function(d){ return line2(d.values); })
                        .attr('stroke-dashoffset', null)
                        .attr('stroke-dasharray', null);

                    // If not, append
                    var linesEnter = lines.enter().append('g')
                        .attr('class', 'term');
                    var contextLinesEnter = contextLines.enter().append('g')
                        .attr('class', 'term');

                    linesEnter.append('g').attr('class', 'terms');
                    var circles = lines.select('g.terms').selectAll('circle').data(function(d){ return d.values; }, function(k){ return ISOFormat(k[0]) + '_' + k[2]; });

                    circles.transition()
                        .attr('r', 2.4)
                        .attr('cx', X)
                        .attr('cy', Y)
                      .transition()
                        .delay(750)
                        .attr('opacity', 1);

                    var pathsEnter = linesEnter.append('path')
                        .attr('clip-path', 'url(#clip)')
                        .attr('class', 'line')
                        .attr('id', function(d){ return d.name.replace(/\s/g, '_'); })
                        .style('stroke', function(d){ return d.color; })
                        .attr('d', function(d){ return line(d.values); })
                        .attr('stroke-dasharray', function(){ var length = this.getTotalLength(); return length + ' ' + length; })
                        .attr('stroke-dashoffset', function(){ return this.getTotalLength(); })
                      .transition()
                        .delay(250)
                        .duration(750)
                        .ease('quad-in-out')
                        .attr('stroke-dashoffset', 0);

                    contextEnter.append('g')
                        .attr('class', 'x brush')
                        .call(brush)
                      .selectAll('rect')
                        .attr('y', -6)
                        .attr('height', (height - margin2.top - margin2.bottom)  + 7);

                    var contextPathsEnter = contextLinesEnter.append('path')
                        .attr('clip-path', 'url(#clip)')
                        .attr('class', 'line')
                        .attr('id', function(d){ return 'context_' + d.name.replace(/\s/g, '_'); })
                        .style('stroke', function(d){ return d.color; })
                        .attr('d', function(d){ return line2(d.values); })
                        .attr('stroke-dasharray', function(){ var length = this.getTotalLength(); return length + ' ' + length; })
                        .attr('stroke-dashoffset', function(){ return this.getTotalLength(); })
                      .transition()
                        .delay(250)
                        .duration(750)
                        .ease('quad-in-out')
                        .attr('stroke-dashoffset', 0);

                    contextEnter.append('g')
                        .attr('class', 'x axis');

                    circles.enter().append('a')
                        .attr('target', '_blank')
                        .attr('xlink:href', function(d){
                            var year = d[0].getFullYear();
                            return 'http://kranten.delpher.nl/nl/results/index/coll/ddd/query//cql/%28date+_gte_+01-01-' +
                                    year + '%29/cql/%28date+_lte_+31-12-' + year +
                                    '%29/cql/%28content+exact+%22' + d[2] + '%22%29';
                            // return '/results?q=' + d[2] + '&date_start=' + year + '-01-01' +
                            //        '&date_end=' + year + '-12-31';
                        })
                      .append('circle')
                        .attr('data-original-title', function(d){
                            $(this).tooltip({
                                container: '#graph',
                                html: true
                            });
                            return 'Year: ' + yearFormat(d[0]) + '<br/>' + 'Frequency: ' + d[3].toLocaleString();
                        })
                        .attr('opacity', 0)
                        .attr('clip-path', 'url(#clip)')
                        .style('fill', function(d){
                            return d[5];
                        })
                        .attr('data-ilpslogging-x', function(d){ return ISOFormat(d[0]); })
                        .attr('data-ilpslogging-y', function(d){ return d[1]; })
                        .attr('data-ilpslogging-query', function(d){ return d[2]; })
                        .on('mouseover', function(d){
                            d3.select(this).transition()
                                .ease('quad-in-out')
                                .attr('r', 5);
                            if (ILPSLogging) {
                                ILPSLogging.logEvent('graph_point_hover', {
                                    x: ISOFormat(d[0]),
                                    y: d[1],
                                    query: d[2]
                                });
                            }
                        })
                        .on('mouseout', function(){
                            d3.select(this).transition()
                                .ease('quad-in-out')
                                .attr('r', 2.4);
                        })
                      .transition()
                        .attr('r', 2.4)
                        .attr('cx', X)
                        .attr('cy', Y)
                      .transition()
                        .delay(750)
                        .attr('opacity', 1);

                    // Update axes
                    g.select('.x.axis')
                        .attr('transform', 'translate(0,' + yScale.range()[0] + ')')
                        .transition()
                        .call(xAxis);

                    g.select('.y.axis')
                        .transition()
                        .call(yAxis);

                    context.select('.x.axis')
                        .attr('transform', 'translate(0,' + yScale2.range()[0] + ')')
                        .transition()
                        .call(xAxis2);

                    lines.exit().remove();

                    contextLines.exit().remove();

                    circles.exit().transition()
                        .attr('opacity', function(){
                            $(this).tooltip('destroy');
                            return 0;
                        })
                        .remove();
                });
            }

            function brushed(){
                xScale.domain(brush.empty() ? xScale2.domain(): brush.extent());
                var xDomain = xScale.domain(),
                    yearRange = xDomain[1].getFullYear() - xDomain[0].getFullYear();

                if (yearRange > 50){
                    xAxis.ticks(d3.time.years, 10);
                }
                else if (yearRange < 50 && yearRange > 20){
                    xAxis.ticks(d3.time.years, 5);
                }
                else if (yearRange < 30){
                    xAxis.ticks(d3.time.years, 1);
                }

                // Find the maximum y value in the selected domain
                var maxYValue = d3.max(ngrams, function(ngram){
                    // for each ngram we have...
                    return d3.max(_.filter(ngram.values, function(v){
                        // ... filter on the values that are in the selected range
                        return (v[0] >= xDomain[0] && v[0] <= xDomain[1]);
                    }), function(v){
                        // Return either rel_freq or freq, depending on the setting
                        if (freqType === 'rel_freq'){
                            return v[4];
                        } else {
                            return v[3];
                        }
                    });
                });

                var maxY = maxYValue * 1.05;
                yScale.domain([0, maxY]);

                d3.selectAll('g.inner g.term path')
                    .attr('stroke-dasharray', null)
                    .attr('stroke-dashoffset', null)
                    .attr('d', function(d){ return line(d.values); });
                d3.selectAll('g.terms circle')
                    .attr('cx', X)
                    .attr('cy', Y);
                d3.select('g.inner .x.axis')/*.transition()*/.call(xAxis);
                d3.select('g.inner .y.axis').transition().duration(50).call(yAxis);
            }

            // x-accessor for path/line generator
            function X(d){
                return xScale(d[0]);
            }

            // y-accessor for path/line generator
            function Y(d){
                return yScale(d[1]);
            }

            function Y2(d){
                return yScale2(d[1]);
            }

            chart.margin = function(val){
                if(!arguments.length) return margin;
                margin = val;
                return chart;
            };

            chart.width = function(val){
                if(!arguments.length) return width;
                width = val;
                return chart;
            };

            chart.height = function(val){
                if(!arguments.length) return height;
                height = val;
                return chart;
            };

            chart.x = function(val){
                if(!arguments.length) return xValue;
                xValue = val;
                return chart;
            };

            chart.y = function(val){
                if(!arguments.length) return yValue;
                yValue = val;
                return chart;
            };

            chart.freqType = function(val){
                if(!arguments.length) return freqType;
                freqType = val;
                return chart;
            };

            chart.contextSize = function(val){
                if(!arguments.length) return contextSize;
                contextSize = val;
                return chart;
            };

            chart.hoverOn = function(line){
                var path = d3.select('path#' + line.id.replace(/\s/g, '_'))
                    .transition()
                    .style('stroke-dashoffset', 0)
                    .style('stroke-width', '2.5px');
                d3.select(path.node().parentNode).selectAll('circle').transition()
                    .attr('cx', X)
                    .attr('cy', Y)
                    .attr('opacity', 1)
                    .attr('r', 3);
            };

            chart.hoverOff = function(line){
                var path = d3.select('path#' + line.id.replace(/\s/g, '_'))
                    .transition()
                    .style('stroke-dashoffset', 0)
                    .style('stroke-width', '1.5px');
                d3.select(path.node().parentNode).selectAll('circle').transition()
                    .attr('cx', X)
                    .attr('cy', Y)
                    .attr('opacity', 1)
                    .attr('r', 2.4);
            };

            return chart;
        }

    });

    var AppRouter = Backbone.Router.extend({
        routes: {
            'q/:terms': 'executeQuery'
        },

        initialize: function() {
            this.vent =  _.extend({}, Backbone.Events);
            this.ngram_coll = new NGramsCollection();

            this.search_bar = new SearchBarView({ collection: this.ngram_coll, vent: this.vent });
            this.legend = new LegendView({ collection: this.ngram_coll, vent: this.vent });
            this.share = new ShareView({ collection: this.ngram_coll, vent: this.vent });
            this.chart = new GraphView({ collection: this.ngram_coll, vent: this.vent });

            this.ngram_coll.on('add', this.addTermQueryUrl, this);
            this.ngram_coll.on('remove', this.removeTermQueryUrl, this);

            this.on('route', function(){
                // Trigger an URLChange event when the user lands first
                // Also, trigger it on the vent, as we're using it in the ShareView
                this.vent.trigger('router:URLChange', Backbone.history.getFragment());
            }, this);

            this.vent.on('router:URLChange', this.sendPageView, this);

            this.url_ngrams = [];
        },

        sendPageView: function(route){
            // Add the pageview to the GA queue
            ga('send', 'pageview', {
                location: Backbone.history.location.href,
                page: '#/' + route
            });
        },

        addTermQueryUrl: function(term) {
            if (this.url_ngrams.indexOf(term.get('id')) !== -1) {
                return;
            }

            var ngrams = this.ngram_coll.pluck('id'),
                url = 'q/' + ngrams.join('|');
            this.navigate(url);
            // Trigger our own event, as the Backbone history API only triggers
            // route changes, not parameter changes
            this.vent.trigger('router:URLChange', url);
        },

        removeTermQueryUrl: function() {
            var ngrams = this.ngram_coll.pluck('id'),
                url = 'q/' + ngrams.join('|');
            this.navigate(url);
            // Trigger our own event, as the Backbone history API only triggers
            // route changes, not parameter changes
            this.vent.trigger('router:URLChange', url);
        },

        executeQuery: function(terms){
            this.url_ngrams = terms.split('|');
            var ngrams = this.ngram_coll.pluck('id').sort();

            var remove_ngrams = [];
            var add_ngrams = [];

            var self = this;
            _.each(this.url_ngrams, function(ngram) {
                if (ngrams.indexOf(ngram) == -1) {
                    add_ngrams.push(ngram);
                }
            });

            _.each(ngrams, function(ngram) {
                if (self.url_ngrams.indexOf(ngram) == -1) {
                    remove_ngrams.push(ngram);
                }
            });

            _.each(add_ngrams, function(ngram) {
                self.search_bar.query(ngram);
            });

            _.each(remove_ngrams, function(ngram) {
                self.legend.removeItem(ngram);
            });
        }
    });

    app = new AppRouter();
    Backbone.history.start();
})(jQuery);
