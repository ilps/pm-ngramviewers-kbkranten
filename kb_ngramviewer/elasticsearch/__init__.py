import elasticsearch

from ..settings import ES_HOSTS

class ElasticsearchService(object):
    """ElasticsearchService that provides an abstraction layer over an ES
    instance"""
    def __init__(self):
        self.es = elasticsearch.Elasticsearch(hosts=ES_HOSTS)

    def search(self, query, index='_all'):
        return self.es.search(body=query, index=index)

    def suggest(self, query):
        return self.es.suggest(body=query)

    def stats(self, *args, **kwargs):
        return self.es.indices.stats(*args, **kwargs)
