DEBUG = True

ES_HOSTS = [
    {'host': 'mashup3.science.uva.nl', 'port': 9200}
]

MINIFY_WEBASSETS = False

# This setting controls the first year of ngrams we return to the client: as
# we don't have data per year, but per decennium or even century before 1840,
# this value is 1840 for now
MIN_YEAR = 1840

# The autocomplete solution can use Levenshtein distance to perform fuzzy
# matching. This is turned off by default, as the corpus contains a lot of
# typos already, which are more likely to be suggested
FUZZY_AUTOCOMPLETE = False

ILPSLOGGING_SERVER = 'http://ilpslogging.staging.dispectu.com'
ILPSLOGGING_PROJECT_KEY = 'p58OaVgDrIDzmCjqtIaHcEqhttJn85DuMATXSEmsQsg'
