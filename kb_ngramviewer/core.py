class NgramviewerError(Exception):
    """Base application error class"""
    def __init__(self, msg, errors=None):
        self.msg = msg
        self.errors = errors

    def __str__(self):
        return repr(self.msg)
