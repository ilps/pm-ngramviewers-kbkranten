from flask.ext.script import Manager
from werkzeug.serving import run_simple

from kb_ngramviewer import frontend

from wsgi import application

manager = Manager(frontend.create_app())

@manager.command
def runserver():
    """Run the Flask development server in debug mode"""
    run_simple('0.0.0.0', 5000, application, use_reloader=True, use_debugger=True)

if __name__ == '__main__':
    manager.run()
